export const convertTimeToTimestamp = (time, date) => {
  // time => "04:22 AM"
  // time => "08:58 PM" 08:58 + 12:00 = 20:58
  // date => "2021-07-06"
  const timeCopy = time.slice().split(" ");
  let convertedTime;

  if (timeCopy[1] === "PM") {
    const timePM = timeCopy[0].split(":");
    timePM[0] = Number(timePM[0]) + 12;
    convertedTime = timePM.join(":");
  } else if (timeCopy[1] === "AM") {
    convertedTime = timeCopy.shift();
  }

  const dateTime = date + " " + convertedTime;
  const timestamp = new Date(dateTime).getTime();
  const time_epoch = timestamp / 1000;

  return time_epoch;
};

export const formattingHour = (time) => {
  let hour = new Date(time).getHours();
  if (!hour.toString()[1]) {
    hour = "0" + hour;
  }
  return hour;
};
