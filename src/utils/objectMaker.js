import sunrise from "../icons/weatherIcons/sunrise.png";
import sunset from "../icons/weatherIcons/sunset.png";
import moment from "moment";

export const sunObject = (time_epoch, astro) => {
  const dateTime = new Date(time_epoch * 1000);
  const time = moment(dateTime).format("YYYY-MM-DD HH:mm");
  let icon;

  switch (astro) {
    case "sunrise":
      icon = sunrise;
      break;
    case "sunset":
      icon = sunset;
      break;
    default:
      break;
  }

  return { time_epoch, time, astro, icon };
};

export const celsiusDegreesObject = {
  type: "c",
  temp: { value: "c", description: "°C" },
  wind: { value: "kph", description: "km/h" },
  precip: { value: "mm", description: "mm" },
  pressure: { value: "mb", description: "hPa" },
};

export const fahrenheitDegreesObject = {
  type: "f",
  temp: { value: "f", description: "°F" },
  wind: { value: "mph", description: "mph" },
  precip: { value: "in", description: "in" },
  pressure: { value: "in", description: "inHg" },
};
