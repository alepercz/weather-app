export const avgDailyPressure = (pressureUnitOfMeasure, day) => {
  // day = forecastDays[indexDay]
  const pressUM = pressureUnitOfMeasure; // mb lub in
  let pressureSum = 0;
  let hourCounter = 0;

  day.hour.forEach(function (hour) {
    pressureSum += hour["pressure_" + pressUM];
    hourCounter += 1;
  });

  return (pressureSum / hourCounter).toFixed(0);
};
