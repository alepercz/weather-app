export const sortArrayOfObjectsByKey = (arr, key) => {
  return arr.sort(function (a, b) {
    let keyA = new Date(a[key]),
      keyB = new Date(b[key]);
    // Compare the 2 dates
    if (keyA < keyB) return -1;
    if (keyA > keyB) return 1;
    return 0;
  });
};
