import "./Error.css";
import sadFace from "../icons/mainIcons/sadFace.png";

const Error = (props) => {
  const { text } = props;

  return (
    <div className="error">
      <img src={sadFace} alt={text} />
      <h1>{text}</h1>
    </div>
  );
};

export default Error;
