import "./DailyWeather.css";
import { motion } from "framer-motion";

const DailyWeather = (props) => {
  const { weatherData, unitsOfMeasure, indexDay } = props;

  const forecastDays = weatherData.forecast.forecastday;
  const tempVal = unitsOfMeasure.temp.value;
  const tempDesc = unitsOfMeasure.temp.description;

  const cartVariants = {
    hover: {
      scale: 1.03,
      boxShadow: "0px 3px 12px 2px #000000",
    },
  };

  return (
    <motion.div
      variants={cartVariants}
      whileHover="hover"
      className="dailyWeather"
    >
      <p>{forecastDays[indexDay].date}</p>
      <img
        src={`https:${forecastDays[indexDay].day.condition.icon}`}
        alt="Daily weather forecast"
      />
      <h1>
        {forecastDays[indexDay].day["avgtemp_" + tempVal].toFixed(0)}
        {tempDesc}
      </h1>
      <div className="dailyMinmax">
        <p>
          Max: {forecastDays[indexDay].day["maxtemp_" + tempVal].toFixed(0)}
          {tempDesc}
        </p>
        <p>
          Min: {forecastDays[indexDay].day["mintemp_" + tempVal].toFixed(0)}
          {tempDesc}
        </p>
      </div>
    </motion.div>
  );
};

export default DailyWeather;
