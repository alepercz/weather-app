import { useContext } from "react";
import "./Week.css";
import DailyWeather from "./DailyWeather";
import { LanguageContext } from "../../LanguageContext";

const Week = (props) => {
  const { weatherData, unitsOfMeasure } = props;
  const language = useContext(LanguageContext);

  return (
    <div className="week">
      <h1>{language.weekSectionTitle}</h1>
      <div className="days">
        {weatherData &&
          weatherData.forecast.forecastday.map((day, i) => (
            <DailyWeather
              key={day.date}
              indexDay={i}
              weatherData={weatherData}
              unitsOfMeasure={unitsOfMeasure}
            />
          ))}
      </div>
    </div>
  );
};

export default Week;
