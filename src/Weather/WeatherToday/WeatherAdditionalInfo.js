import "./WeatherAdditionalInfo.css";
import "../../App.css";
import chanceOfRain from "../../icons/weatherIcons/chanceOfRain.png";
import rainfall from "../../icons/weatherIcons/rainfall.png";
import wind from "../../icons/weatherIcons/wind.png";
import humidity from "../../icons/weatherIcons/humidity.png";
import pressure from "../../icons/weatherIcons/pressure.png";
import feelsLike from "../../icons/weatherIcons/feelsLike.png";
import { avgDailyPressure } from "../../utils/avgDailyPressure";

const Information = (props) => {
  const { src, alt, text } = props;
  return (
    <div className="information">
      <img src={src} alt={alt} />
      <p className="whiteBg">{text}</p>
    </div>
  );
};

const WeatherRestInfo = (props) => {
  const { forecastDays, currentWeather, unitsOfMeasure, indexDay } = props;

  const precipVal = unitsOfMeasure.precip.value;
  const precipDesc = unitsOfMeasure.precip.description;
  const windVal = unitsOfMeasure.wind.value;
  const windDesc = unitsOfMeasure.wind.description;
  const pressureVal = unitsOfMeasure.pressure.value;
  const pressureDesc = unitsOfMeasure.pressure.description;
  const tempVal = unitsOfMeasure.temp.value;
  const tempDesc = unitsOfMeasure.temp.description;

  return (
    <section className="informations">
      <div className="topic" name="rain">
        <Information
          src={chanceOfRain}
          alt={`${forecastDays[indexDay].day.daily_chance_of_rain}% chance of rain`}
          text={forecastDays[indexDay].day.daily_chance_of_rain + "%"}
        />
        <Information
          src={rainfall}
          alt={`Total precipitation ${
            forecastDays[indexDay].day["totalprecip_" + precipVal]
          } ${precipDesc}`}
          text={
            forecastDays[indexDay].day["totalprecip_" + precipVal] + precipDesc
          }
        />
      </div>
      <div className="topic" name="wind and humidity">
        <Information
          src={wind}
          alt={`Maximum wind speed ${
            forecastDays[indexDay].day["maxwind_" + windVal]
          } ${windDesc}`}
          text={forecastDays[indexDay].day["maxwind_" + windVal] + windDesc}
        />
        <Information
          src={humidity}
          alt={`Average humidity ${forecastDays[indexDay].day.avghumidity}`}
          text={forecastDays[indexDay].day.avghumidity + "%"}
        />
      </div>
      <div className="topic" name="pressure and perceived temperature">
        <Information
          src={pressure}
          alt={`Average daily pressure is ${avgDailyPressure(
            pressureVal,
            forecastDays[indexDay]
          )} ${pressureDesc}`}
          text={
            avgDailyPressure(pressureVal, forecastDays[indexDay]) + pressureDesc
          }
        />
        <Information
          src={feelsLike}
          alt={`Feels like ${
            currentWeather["feelslike_" + tempVal]
          }${tempDesc}`}
          text={currentWeather["feelslike_" + tempVal].toFixed(0) + tempDesc}
        />
      </div>
    </section>
  );
};

export default WeatherRestInfo;
