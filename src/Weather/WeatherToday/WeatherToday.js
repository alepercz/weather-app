import "./WeatherToday.css";
import WeatherHourly from "./WeatherHourly";
import WeatherAdditionalInfo from "./WeatherAdditionalInfo";

const WeatherToday = (props) => {
  const {
    weatherData,
    unitsOfMeasure,
    indexDay,
    numOfHours,
    eachDaySeparately,
  } = props;

  const temp = unitsOfMeasure.temp;
  const forecastDays = weatherData.forecast.forecastday;
  const currentWeather = weatherData.current;

  return (
    <div className="weatherToday">
      <WeatherHourly
        forecastDays={forecastDays}
        temp={temp}
        indexDay={indexDay}
        numOfHours={numOfHours}
        eachDaySeparately={eachDaySeparately}
      />
      <WeatherAdditionalInfo
        forecastDays={forecastDays}
        currentWeather={currentWeather}
        unitsOfMeasure={unitsOfMeasure}
        indexDay={indexDay}
      />
    </div>
  );
};

export default WeatherToday;
