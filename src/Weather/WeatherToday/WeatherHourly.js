import { useContext } from "react";
import "./WeatherHourly.css";
import { sortArrayOfObjectsByKey } from "../../utils/sortArrayOfObjectsByKey";
import { sunObject } from "../../utils/objectMaker";
import {
  formattingHour,
  convertTimeToTimestamp,
} from "../../utils/formattingTime";
import { LanguageContext } from "../../LanguageContext";

const WeatherHourly = (props) => {
  const { forecastDays, temp, indexDay, numOfHours, eachDaySeparately } = props;
  const language = useContext(LanguageContext);

  const tempVal = temp.value;
  const tempDesc = temp.description;
  const nextDay = indexDay + 1;

  const sunriseTimestamp = convertTimeToTimestamp(
    forecastDays[indexDay].astro.sunrise,
    forecastDays[indexDay].date
  );
  const sunsetTimestamp = convertTimeToTimestamp(
    forecastDays[indexDay].astro.sunset,
    forecastDays[indexDay].date
  );

  const sunriseToday = sunObject(sunriseTimestamp, "sunrise");
  const sunsetToday = sunObject(sunsetTimestamp, "sunset");
  const today = forecastDays[indexDay]?.hour;
  const tommorow = forecastDays[nextDay]?.hour;

  let todayTommorow = today.concat(sunriseToday, sunsetToday, tommorow);
  todayTommorow = sortArrayOfObjectsByKey(todayTommorow, "time_epoch");

  let days;
  if (eachDaySeparately) {
    days = todayTommorow;
  } else {
    days = todayTommorow.filter(
      (hour) =>
        new Date(hour.time).getHours() >= new Date().getHours() ||
        new Date(hour.time).getDay() > new Date().getDay()
    );
  }

  const hours = Array(numOfHours).fill(0);
  for (let i in hours) {
    hours[i] = days[i];
  }
  // console.log(hours);

  return (
    forecastDays && (
      <div className="forecastDay">
        {hours.map((hour, i) => (
          <div key={i} className="forecastHour">
            {hour.astro ? (
              <>
                <h3>
                  {new Date().getTime() === new Date(hour.time_epoch) * 1000
                    ? language.sayNow
                    : hour.time.split(" ")[1]}
                </h3>
                <img
                  className={hour.astro}
                  src={hour.icon}
                  alt={`Weather forecast for ${
                    hour.time.split(" ")[1]
                  } o'clock`}
                />
              </>
            ) : (
              <>
                <h3>
                  {new Date().getHours() === new Date(hour.time).getHours()
                    ? language.sayNow
                    : formattingHour(hour.time)}
                </h3>
                <img
                  src={`https:${hour.condition.icon}`}
                  alt={`Weather forecast for ${formattingHour(
                    hour.time
                  )} o'clock`}
                />
                <h3>
                  {hour["temp_" + tempVal].toFixed(0)}
                  {tempDesc}
                </h3>
              </>
            )}
          </div>
        ))}
      </div>
    )
  );
};

export default WeatherHourly;
