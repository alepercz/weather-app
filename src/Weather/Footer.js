import "./Footer.css";
import heartRed from "../icons/mainIcons/heartRed.png";

const Footer = () => {
  return (
    <div className="footer">
      Made with
      <img src={heartRed} alt="Love" />
      by Aleksandra Neugebauer
    </div>
  );
};

export default Footer;
