import React from "react";
import { motion } from "framer-motion";
import "./ChangeLanguage.css";
import poland from "../../icons/mainIcons/poland.png";
import unitedKingdom from "../../icons/mainIcons/unitedKingdom.png";

const spring = {
  type: "spring",
  stiffness: 700,
  damping: 30,
};

const ChangeLanguage = (props) => {
  const { isOn, toggleSwitch } = props;

  return (
    <div className="switch" data-ison={isOn} onClick={toggleSwitch}>
      <motion.img
        className="handle"
        layout
        transition={spring}
        src={isOn ? unitedKingdom : poland}
        alt="Language"
      />
    </div>
  );
};

export default ChangeLanguage;
