import "./Favorites.css";
import heart from "../../icons/mainIcons/heart.png";
import heartRedFilled from "../../icons/mainIcons/heartRedFilled.png";
import { motion } from "framer-motion";

const Favorites = (props) => {
  const { onClick, isFavorite } = props;

  const buttonVariants = {
    clicked: {
      opacity: 0.5,
      scale: 0.9,
    },
  };

  return (
    <div className="favorites">
      <motion.img
        src={isFavorite ? heartRedFilled : heart}
        alt="Favorites"
        onClick={onClick}
        variants={buttonVariants}
        whileTap="clicked"
      />
    </div>
  );
};

export default Favorites;
