import { useState, useContext } from "react";
import "./Header.css";

import SearchLocation from "./SearchLocation";
import UnitsOfMeasureSelector from "./UnitsOfMeasure";
import ChangeLanguage from "./ChangeLanguage";
import Favorites from "./Favorites";
import SavedFavorites from "./SavedFavorites";

import { AnimatePresence, motion } from "framer-motion";
import { LanguageContext } from "../../LanguageContext";

const Header = (props) => {
  const {
    input,
    searchHandleChange,
    searchLocationHandler,
    unitsOfMeasureHandleChange,
    unitsOfMeasure,
    isOn,
    toggleSwitch,
    addToFavoritesHandler,
    isFavorite,
    toggleSideBar,
  } = props;
  const [showPreferences, setShowPreferences] = useState(false);
  const language = useContext(LanguageContext);

  const unitsOfMeasureHandler = () => {
    setShowPreferences((show) => !show);
  };

  const buttonVariants = {
    hover: {
      scale: 1.05,
    },
  };

  return (
    <>
      <div className="header">
        <h1 onClick={() => window.location.reload(false)}>myWeatherApp</h1>
        <div>
          <ChangeLanguage isOn={isOn} toggleSwitch={toggleSwitch} />
          <Favorites isFavorite={isFavorite} onClick={addToFavoritesHandler} />
          <motion.button
            variants={buttonVariants}
            whileHover="hover"
            className="headerBtn"
            onClick={unitsOfMeasureHandler}
          >
            {showPreferences ? language.confirmButton : language.changeButton}
          </motion.button>
          <SearchLocation
            input={input}
            handleChange={searchHandleChange}
            searchLocationHandler={searchLocationHandler}
          />
          <SavedFavorites toggleSideBar={toggleSideBar} />
        </div>
      </div>
      <AnimatePresence>
        {showPreferences && (
          <UnitsOfMeasureSelector
            handleChange={unitsOfMeasureHandleChange}
            unitsOfMeasure={unitsOfMeasure}
          />
        )}
      </AnimatePresence>
    </>
  );
};

export default Header;
