import { useContext } from "react";
import "./UnitsOfMeasure.css";
import { motion } from "framer-motion";
import { LanguageContext } from "../../LanguageContext";

const UnitsOfMeasureSelector = (props) => {
  const { handleChange, unitsOfMeasure } = props;
  const language = useContext(LanguageContext);
  const unitType = unitsOfMeasure.type;

  const dropdownVariants = {
    hidden: {
      opacity: 0,
      height: 0,
    },
    visible: {
      opacity: 1,
      height: 100,
    },
    exit: {
      opacity: 0,
      height: 0,
      transition: {
        when: "afterChildren",
      },
    },
  };

  const childrenVariants = {
    hidden: {
      opacity: 0,
    },
    visible: {
      opacity: 1,
    },
    exit: {
      opacity: 0,
    },
  };

  return (
    <motion.div
      variants={dropdownVariants}
      initial="hidden"
      animate="visible"
      exit="exit"
      className="unitsOfMeasure"
      onChange={handleChange}
    >
      <motion.div variants={childrenVariants}>
        <div className="topic">
          <input
            type="radio"
            name="unitsOfMesure"
            value="c"
            defaultChecked={unitType === "c"}
          />
          <label>{language.systemSI} (°C)</label>
        </div>
        <div className="topic">
          <input
            type="radio"
            name="unitsOfMesure"
            value="f"
            defaultChecked={unitType === "f"}
          />
          <label>{language.britishSystem} (°F)</label>
        </div>
      </motion.div>
    </motion.div>
  );
};

export default UnitsOfMeasureSelector;
