import "./SavedFavorites.css";
import saved from "../../icons/mainIcons/saved.png";
import { motion } from "framer-motion";

const SavedFavorites = (props) => {
  const { toggleSideBar } = props;

  const buttonVariants = {
    clicked: {
      opacity: 0.5,
      scale: 0.9,
    },
  };
  return (
    <div className="savedFavorites">
      <motion.img
        src={saved}
        alt="Saved Favorites Places"
        onClick={toggleSideBar}
        variants={buttonVariants}
        whileTap="clicked"
      />
    </div>
  );
};

export default SavedFavorites;
