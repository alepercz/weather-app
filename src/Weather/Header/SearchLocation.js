import { useContext } from "react";
import "./SearchLocation.css";
import search from "../../icons/mainIcons/search.png";
import { LanguageContext } from "../../LanguageContext";
import { motion } from "framer-motion";

const SearchLocation = (props) => {
  const { input, searchLocationHandler, handleChange } = props;
  const language = useContext(LanguageContext);

  const buttonVariants = {
    clicked: {
      opacity: 0.5,
      scale: 0.9,
    },
  };

  return (
    <div className="searchLocation">
      <input
        type="text"
        name="search location"
        value={input}
        onChange={handleChange}
        placeholder={language.searchPlaceholder}
      />
      <button onClick={searchLocationHandler}>
        <motion.img
          src={search}
          alt="Search"
          variants={buttonVariants}
          whileTap="clicked"
        />
      </button>
    </div>
  );
};

export default SearchLocation;
