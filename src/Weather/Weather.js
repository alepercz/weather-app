import { useEffect, useState, useContext } from "react";
import "./Weather.css";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";

import Loader from "react-loader-spinner";
import Error from "./Error";
import Header from "./Header/Header";
import BackgroundGradientMaker from "./BackgroundGradientMaker";
import CurrentWeather from "./CurrentWeather";
import WeatherToday from "./WeatherToday/WeatherToday";
import Week from "./Week/Week";

import { LanguageContext } from "../LanguageContext";
import {
  celsiusDegreesObject,
  fahrenheitDegreesObject,
} from "../utils/objectMaker";
import Footer from "./Footer";

const Weather = (props) => {
  const {
    isOn,
    updateIsOn,
    myFavoriteCities,
    setMyFavoriteCities,
    location,
    setLocation,
    toggleSideBar,
  } = props;
  const [weatherData, setWeatherData] = useState();
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState({});
  const [input, setInput] = useState("");
  const [unitsOfMeasure, setUnitsOfMeasure] = useState(celsiusDegreesObject);
  const [isFavorite, setIsFavorite] = useState([]);

  const language = useContext(LanguageContext);
  // console.log("error", error);

  useEffect(() => {
    const myMemory = localStorage.getItem("myFavoriteCities");
    if (myMemory) {
      const value = JSON.parse(myMemory);
      setMyFavoriteCities(value);
    }
    navigator.geolocation.getCurrentPosition(
      function (position) {
        const coordsLat = position.coords.latitude;
        const coordsLong = position.coords.longitude;
        setLocation(`${coordsLat},${coordsLong}`);
      },
      function (error) {
        console.error("Error Code = " + error.code + " - " + error.message);
        setError({ status: true, code: error.code });
      }
    );
  }, []);

  useEffect(() => {
    const newValue = JSON.stringify(myFavoriteCities);
    localStorage.setItem("myFavoriteCities", newValue);
  }, [myFavoriteCities]);

  useEffect(() => {
    const timeInterval = 1000 * 60 * 60; //godzina
    getWeatherData(location);
    const interval = setInterval(() => {
      getWeatherData(location);
      console.log("Odświezyło");
    }, timeInterval);
    return () => clearInterval(interval);
  }, [location]);

  useEffect(() => {
    if (weatherData && myFavoriteCities.includes(weatherData.location.name)) {
      setIsFavorite(true);
    } else {
      setIsFavorite(false);
    }
  }, [myFavoriteCities, weatherData]);

  const getWeatherData = (location) => {
    setIsLoading(true);
    fetch(
      `https://api.weatherapi.com/v1/forecast.json?key=209bad3d44ff4178a0e132331212906&q=${location}&days=7&aqi=no&alerts=no`
    )
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        if (data?.error) {
          setError({ status: true, code: data.error.code });
        } else {
          setWeatherData(data);
          setError({ status: false, code: "" });
        }
      })
      .catch((err) => {
        setError({ status: true, code: err.code });
        console.log("err", err);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  const searchHandleChange = (event) => {
    setInput(event.target.value);
  };

  const searchLocationHandler = () => {
    if (input) {
      setLocation(input);
      setInput("");
    } else {
      alert(language.alert);
    }
  };

  const unitsOfMeasureHandleChange = (event) => {
    if (event.target.value === "c") {
      setUnitsOfMeasure(celsiusDegreesObject);
    } else if (event.target.value === "f") {
      setUnitsOfMeasure(fahrenheitDegreesObject);
    }
  };

  const toggleSwitch = () => updateIsOn((isOn) => !isOn);

  const addToFavoritesHandler = () => {
    if (error.status) return;
    if (myFavoriteCities.includes(weatherData.location.name)) {
      setMyFavoriteCities((locations) =>
        locations.filter((location) => location !== weatherData.location.name)
      );
    } else {
      setMyFavoriteCities((locations) => [
        ...new Set([...locations, weatherData.location.name]),
      ]);
    }
  };

  if (!weatherData) return null;

  return (
    <>
      <Header
        input={input}
        searchHandleChange={searchHandleChange}
        searchLocationHandler={searchLocationHandler}
        unitsOfMeasureHandleChange={unitsOfMeasureHandleChange}
        unitsOfMeasure={unitsOfMeasure}
        isOn={isOn}
        toggleSwitch={toggleSwitch}
        addToFavoritesHandler={addToFavoritesHandler}
        isFavorite={isFavorite}
        toggleSideBar={toggleSideBar}
      />
      {isLoading ? (
        <div className="loader">
          <Loader
            type="Puff"
            color="rgba(98, 150, 254, 0.877)"
            height={200}
            width={200}
            timeout={3000} //3 sec
          />
        </div>
      ) : error.status ? (
        <Error
          text={error.code === 1006 ? language.error1006 : language.error}
        />
      ) : (
        <>
          <BackgroundGradientMaker currentWeather={weatherData.current}>
            <CurrentWeather
              weatherData={weatherData}
              unitsOfMeasure={unitsOfMeasure}
              indexDay={0}
            />
            <WeatherToday
              weatherData={weatherData}
              unitsOfMeasure={unitsOfMeasure}
              indexDay={0}
              numOfHours={12}
              eachDaySeparately={false}
            />
          </BackgroundGradientMaker>
          <Week weatherData={weatherData} unitsOfMeasure={unitsOfMeasure} />
          <Footer />
        </>
      )}
    </>
  );
};

export default Weather;
