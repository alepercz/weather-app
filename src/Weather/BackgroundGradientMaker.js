import { useState, useEffect } from "react";
import "./BackgroundGradientMaker.css";
import { backgroundNameFromCode } from "../utils/backgroundCodes";

const BackgroundGradientMaker = (props) => {
  const { currentWeather } = props;
  const [backgroundGradient, setBackgroundGradient] = useState("clearDay");
  const [is_day, setIs_day] = useState(1);
  const [code, setCode] = useState(1000);

  useEffect(() => {
    if (currentWeather) {
      setIs_day(currentWeather.is_day);
      setCode(currentWeather.condition.code);
    }
  }, [currentWeather]);

  useEffect(() => {
    setBackgroundGradient(backgroundNameFromCode(code, is_day));
  }, [code, is_day]);

  return (
    <div className={`${backgroundGradient} fontColor`}>{props.children}</div>
  );
};

export default BackgroundGradientMaker;
