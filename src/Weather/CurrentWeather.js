import "./CurrentWeather.css";
import "../App.css";

const CurrentWeather = (props) => {
  const { weatherData, unitsOfMeasure, indexDay } = props;

  const locationInfo = weatherData.location;
  const currentWeather = weatherData.current;
  const forecastDays = weatherData.forecast.forecastday;
  const tempVal = unitsOfMeasure.temp.value;
  const tempDesc = unitsOfMeasure.temp.description;

  return (
    <div className="currentWeather">
      <h1>{locationInfo.name}</h1>
      <img
        src={`https:${currentWeather.condition.icon}`}
        alt="Current weather forecast"
      />
      <h1>
        {currentWeather["temp_" + tempVal].toFixed(0)}
        {tempDesc}
      </h1>
      <div className="minmax">
        <p className="whiteBg">
          Max: {forecastDays[indexDay].day["maxtemp_" + tempVal].toFixed(0)}
          {tempDesc}
        </p>
        <p className="whiteBg">
          Min: {forecastDays[indexDay].day["mintemp_" + tempVal].toFixed(0)}
          {tempDesc}
        </p>
      </div>
    </div>
  );
};

export default CurrentWeather;
