import { createContext } from "react";

export const themes = {
  polish: {
    confirmButton: "Zatwierdź",
    changeButton: "Wybierz jednostki miary",
    searchPlaceholder: "Wpisz nazwę miejscowości",
    sayNow: "Teraz",
    systemSI: "międzynarodowy układ jednostek miar",
    britishSystem: "amerykański system metryczny",
    weekSectionTitle: "Pogoda na najbliższe dni",
    favorites: "Brak ulubionych miejsc",
    alert: "Proszę wpisać nazwę miejscowości",
    error: "Ups! Coś poszło nie tak...",
    error1006: "Proszę wpisać poprawnie nazwę lub współrzędne miejscowości",
  },
  english: {
    confirmButton: "Confirm",
    changeButton: "Select units of measure",
    searchPlaceholder: "Enter the name of the city",
    sayNow: "Now",
    systemSI: "International system of units",
    britishSystem: "the American metric system",
    weekSectionTitle: "Weather for the next few days",
    favorites: "No favorite places",
    alert: "Please enter the name of the city",
    error: "Ups! Something went wrong...",
    error1006: "Please enter correctly name or coordinates of the city",
  },
};

export const LanguageContext = createContext({ themes: themes.polish });
