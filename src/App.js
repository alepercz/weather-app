import { useState } from "react";
import "./App.css";
import Weather from "./Weather/Weather";
import ListFavorites from "./ListFavorites";
import { LanguageContext, themes } from "./LanguageContext";
import { AnimatePresence } from "framer-motion";

function App() {
  const [isOn, setIsOn] = useState(false);
  const [isOpen, setIsOpen] = useState(false);
  const [myFavoriteCities, setMyFavoriteCities] = useState([]);
  const [location, setLocation] = useState();

  const toggleSideBar = () => {
    setIsOpen((open) => !open);
  };

  return (
    <div className="App">
      <LanguageContext.Provider value={isOn ? themes.english : themes.polish}>
        <Weather
          isOn={isOn}
          updateIsOn={setIsOn}
          updateIsOpen={setIsOpen}
          myFavoriteCities={myFavoriteCities}
          setMyFavoriteCities={setMyFavoriteCities}
          location={location}
          setLocation={setLocation}
          toggleSideBar={toggleSideBar}
        />
        <AnimatePresence>
          {isOpen && (
            <ListFavorites
              items={myFavoriteCities}
              updateLocation={setLocation}
              toggleSideBar={toggleSideBar}
            />
          )}
        </AnimatePresence>
      </LanguageContext.Provider>
    </div>
  );
}

export default App;
