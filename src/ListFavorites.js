import { useContext } from "react";
import "./ListFavorites.css";
import collapse from "./icons/mainIcons/collapse.png";
import { LanguageContext } from "./LanguageContext";
import { motion } from "framer-motion";

const ListFavorites = (props) => {
  const { items, updateLocation, toggleSideBar } = props;
  const language = useContext(LanguageContext);

  const sideBarVariants = {
    startAt: { opacity: 0, x: 100 },
    moveIn: { opacity: 1, x: 0, transition: { duration: 0.5 } },
    moveOut: { opacity: 0, x: 100, transition: { duration: 0.5 } },
  };

  return (
    <motion.div
      className="sidebar"
      variants={sideBarVariants}
      initial="startAt"
      animate="moveIn"
      exit="moveOut"
    >
      <div className="items">
        <img src={collapse} alt="Collapse" onClick={toggleSideBar} />
        {items.length === 0 ? (
          <p>{language.favorites}</p>
        ) : (
          items.map((item) => (
            <p className="item" key={item} onClick={() => updateLocation(item)}>
              {item}
            </p>
          ))
        )}
      </div>
    </motion.div>
  );
};

export default ListFavorites;
